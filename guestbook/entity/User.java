package www.bt.com.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
@NamedQueries({
	@NamedQuery(
	name = "findbyUsernameandpassword",
	query = "from User u where u.userName = :userName and u.password=:password"
	)
})

@Entity
@Table(name="USERS")
public class User implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	private Long userId ;
	
	@Column(name = "userName")
	private String userName ;
	
	@Column(name = "password")
	private String password;
	@Column(name = "role")
	private String role ;
	
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	public void setRole(String role) {
		this.role = role;
	}
	public Long getUserId() {
		return userId;
	}
	public String getPassword() {
		return password;
	}
	public String getUserName() {
		return userName;
	}
	public String getRole() {
		return role;
	}
	 /**
     * Constructs a <code>String</code> with all attributes
     * in name = value format.
     *
     * @return a <code>String</code> representation
     * of this object.
     */
    @Override
    public final String toString() {

	final String TAB = "  ";

    StringBuilder retValue = new StringBuilder();

    retValue.append("Login ( ")
        .append(super.toString()).append(TAB)
        .append("userId = ").append(this.userId).append(TAB)
        .append("userName = ").append(this.userName).append(TAB)
        .append("password = ").append(this.password).append(TAB)
        .append("role = ").append(this.role).append(TAB)
        .append(" )");

    return retValue.toString();
    }
	
}
