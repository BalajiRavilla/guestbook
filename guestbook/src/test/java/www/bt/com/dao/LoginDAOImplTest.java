package www.bt.com.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import www.bt.com.entity.User;

public class LoginDAOImplTest extends AbstractDaoTest{
	
    @Autowired
    private LoginDAO LoginDAO;
    @Autowired
    private JdbcTemplate jdbcTemplateObject;
    private static String STRING_USER_NAME="balaji";
    private static String STRING_PASSWORD="balaji";
    private static final Integer INTEGER1 = 1;
    private static String ROLE="ROLE_USER";
    
    @Before
    public void setupData()
    {
    	
    	this.jdbcTemplateObject.update("delete from USERS");
    	this.jdbcTemplateObject.update(
                "insert into USERS(USER_ID, USERNAME"
                    + ",PASSWORD, ROLE) values(?,"
                    + " ?,?,?)" , INTEGER1, STRING_USER_NAME, STRING_PASSWORD,
                    ROLE);

    }
    
    @Test
    public void testLogin()
    {
       List<User> user = this.LoginDAO.login(STRING_USER_NAME, STRING_PASSWORD);
        assertNotNull(user);
        assertEquals(user.get(0).getRole(), ROLE);
    }
     
    
     


}
