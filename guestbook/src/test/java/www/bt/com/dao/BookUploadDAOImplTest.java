package www.bt.com.dao;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;

import www.bt.com.entity.Reviews;
import www.bt.com.entity.User;

public class BookUploadDAOImplTest extends AbstractDaoTest{
	
    @Autowired
    private ReviewsDAO bookUploadDAO;
    @Autowired
    private JdbcTemplate jdbcTemplateObject;
    private static String STRING_FILE_NAME="Ext JS 4 First Look";
    private static String STRING_FILE_NAME1="Ext JS 4 First Look1";
    private static String STRING_PASSWORD="balaji";
    private static final Long INTEGER1 = 1L;
    private static String ROLE="ROLE_USER";

   private byte[]  fileRead() {
       File file = new File("src/test/resources/test.txt");
       byte[] bFile = new byte[(int) file.length()];

       try {
           FileInputStream fileInputStream = new FileInputStream(file);
           fileInputStream.read(bFile);
           fileInputStream.close();
       } catch (Exception e) {
           e.printStackTrace();
       }
         return bFile;

   }
    
    @Before
    public void setupData()
    {
    	this.jdbcTemplateObject.update("delete from BOOK_UPLOAD");
    	this.jdbcTemplateObject.update(
                "insert into BOOK_UPLOAD(FILE_ID,"
                    + "FILE_NAME, FILE_DATA) values(?,"
                    + " ?,?)" , INTEGER1, STRING_FILE_NAME, this.fileRead());

    }
    
    @Test
    public void testSave()
    {
    	
    	Reviews book = new Reviews();
        book.setName("Ext JS 4 First Look");
        book.setData(this.fileRead());
        bookUploadDAO.save(book);
        assertNotNull(book.getId());
        assertEquals(book.getName(),STRING_FILE_NAME );

    }
     
    @Test
    public void testDelete()
    {
    	
    	Reviews book = new Reviews();
    	book.setId(INTEGER1);
        book.setName("Ext JS 4 First Look");
        book.setData(this.fileRead());
        bookUploadDAO.delete(book);

    }

    @Test
    public void testView()
    {
    	
       List<Reviews> list=bookUploadDAO.view();
       for (Reviews bookUpload: list)
        try{
            //FileOutputStream fos = new FileOutputStream("images\\output.jpg");  //windows
            FileOutputStream fos = new FileOutputStream("src/test/resources/output.txt");
            fos.write(bookUpload.getPhoto());
            fos.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    }