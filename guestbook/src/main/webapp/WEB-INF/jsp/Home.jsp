<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<head>
		<title>GUEST BOOK</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	</head>
	<body>
		<div class="container">
			<h1>Guest Book</h1>
			<div class="form-group">
				<form action="Logout" style="float:right;" method="post">
					<input type="submit" class="btn btn-info" value="Logout" />
				</form>
			</div>
			<br/><br/>
			<form action="manage_reviews" method="post">
				<div class="table-responsive form-group">
					<table class="table table-bordered">
						<col width="5%"/>
  						<col width="15%"/>
  						<col width="70%"/>
						<col width="10%"/>
						<thead class="thead-dark">
							<tr>
								<th>Select</th>
								<th>Guest Name</th>
								<th colspan="2">Reviews</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${user.reviews}" var="review">
										<input type="hidden" class="form-control" name="reviewID" value="${review.reviewID}">
							
								<tr>
									<td>
										<input type="radio" name="guest" value="${user.displayName}"/>
									</td>
									<td>${user.displayName}</td>
									<td>
										<input class="form-control" type="text" name="review" value="${review.reviewCommnets}" ${user.role == 'ROLE_USER' ? 'readonly': ''}/>
									</td>
									<td>
										<div class="thumbnail">
											<a href="${review.imagePath}" target="_blank">
											  <img src="${review.imagePath}" style="width:20%">
											</a>
										</div>
									</td>
								</tr>
							</c:forEach>
							<tr>
								<td>
									<input type="radio" name="guest" value="Ankan"/>
								</td>
								<td>Ankan</td>
								<td>
									<input class="form-control" type="text" name="review" value="Nice Restaurant!!!" readonly="readonly"/>
								</td>
								<td>
									<div class="thumbnail">
										<a href="https://placeimg.com/110/110/abstract/1" target="_blank">
										  <img src="https://placeimg.com/110/110/abstract/1" style="width:20%">
										</a>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="form-group">
					<center>
						<c:if test="${user.role == 'ROLE_ADMIN'}" >
							<input type="submit" class="btn btn-info" name="submitButton" value="Approve" />
							<input type="submit" class="btn btn-info" name="submitButton" value="Delete" />
						</c:if>
						<c:if test="${user.role == 'ROLE_USER'}" >
							<input type="submit" class="btn btn-info" name="submitButton" value="Edit" />
						</c:if>
					</center>
				</div>
			</form>
			<form action="add_reviews" method="post" enctype="multipart/form-data">
			<input type="hidden" class="form-control" name="userId" value="${user.userID}">
			<input type="hidden" class="form-control" name="userName" value="${user.userName}">
			
				<br/><br/>
					<div class="form-group">
						<input type="text" class="form-control" name="review" placeholder="Review">
					</div>
					<div class="form-group">
						<input type="file" id="myFile" name="filename">
					</div>
					<div class="form-group">
						<button class="btn btn-info" type="submit">Submit</button>
					</div>
			</form>
		</div>
	</body>
</html>