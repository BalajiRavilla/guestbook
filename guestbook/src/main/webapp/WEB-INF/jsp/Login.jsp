<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
	<head>
		<title>GUEST BOOK</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	</head>
	<body>
		<div class="container">
			<h1>Guest Book Login</h1>
			<div class="jumbotron" style="width:30%;height:40%">
			<form:form name="userForm" modelAttribute="user" action="processLogin" method="post">
					<div class="form-group">
					<font color="red"><b>${message}</b></font><br/>
					  <label for="userName">USERNAME</label>
					  <input type="text" class="form-control" id="userName" name="userName">
					  <font color="red"> <form:errors path="userName"></form:errors></font><br/> 
					  <label for="password">PASSWORD</label>
					  <input type="password" class="form-control" id="password" name="password">
					  <font color="red"> <form:errors path="password"></form:errors></font><br/>
					</div>
					<input type="submit" class="btn btn-info" value="Login" />
					<p>Don't have account yet?</p>
					<input type="submit" class="btn btn-info" value="Create Account" />
				</form:form>
			</div>
		</div>
	</body>
</html>