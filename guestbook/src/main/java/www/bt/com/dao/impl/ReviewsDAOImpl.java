package www.bt.com.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import www.bt.com.dao.ReviewsDAO;
import www.bt.com.entity.Reviews;

public class ReviewsDAOImpl implements ReviewsDAO  {
	@Autowired
	SessionFactory sessionFactory;

		private Session openSession()
	{
		return this.sessionFactory.openSession();
	}
	
	public void save(Reviews bookUpload) {
		this.openSession().saveOrUpdate(bookUpload);
	}
	public void update(Reviews reviews)
	{
		this.openSession().update(reviews);
	}
	public void delete(Reviews bookUpload)
	{
		this.openSession().delete(bookUpload);
	}
	@SuppressWarnings("unchecked")
	public List<Reviews> view()
	{
		
		return (List<Reviews>)this.openSession().createQuery("from FileUpload").list();
	}
	

}
