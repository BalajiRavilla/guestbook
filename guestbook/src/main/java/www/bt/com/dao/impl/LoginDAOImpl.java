package www.bt.com.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import www.bt.com.dao.LoginDAO;
import www.bt.com.entity.User;

public class LoginDAOImpl implements LoginDAO  {
	@Autowired
    SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	@SuppressWarnings("unchecked")
	public List<User> login(String userName, String password) {
		Query query = sessionFactory.openSession().createQuery("from User u where u.userName = :userName and u.password=:password")
				.setString("userName", userName).setString("password", password);
		return (List<User>)query.list();
	}

}
