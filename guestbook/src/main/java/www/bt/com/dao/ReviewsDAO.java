package www.bt.com.dao;

import java.util.List;

import www.bt.com.entity.Reviews;

public interface ReviewsDAO {
	void save(Reviews reviews);
	void update(Reviews reviews);
	void delete(Reviews reviews);
	List<Reviews> view();
}
