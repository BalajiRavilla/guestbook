package www.bt.com.dao;

import java.util.List;

import www.bt.com.entity.User;

public interface LoginDAO {
	
	List<User> login( String userName, String password);
}
