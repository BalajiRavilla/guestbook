package www.bt.com.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import www.bt.com.dao.UserDao;
import www.bt.com.entity.User;

public class UserDaoImpl implements UserDao{
	@Autowired
    SessionFactory sessionFactory;
	
	private Session openSession()
	{
		return this.sessionFactory.openSession();
	}
	
	
	public void save(User user) {
		this.openSession().saveOrUpdate(user);
		
	}

}
