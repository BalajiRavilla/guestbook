package www.bt.com.model;

import java.util.Date;

import www.bt.com.entity.User;

public class Reviews {
    private Long reviewID;
    public Long getReviewID() {
		return reviewID;
	}
	public void setReviewID(Long reviewID) {
		this.reviewID = reviewID;
	}
	public String getReviewCommnets() {
		return reviewCommnets;
	}
	public void setReviewCommnets(String reviewCommnets) {
		this.reviewCommnets = reviewCommnets;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public char getApprovedFlag() {
		return approvedFlag;
	}
	public void setApprovedFlag(char approvedFlag) {
		this.approvedFlag = approvedFlag;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	private String reviewCommnets;
    private String imagePath;
	private User user;
    private char approvedFlag;
	private Date creationDate;
	
	@Override
	public String toString() {
		return "Reviews [reviewID=" + reviewID + ", reviewCommnets=" + reviewCommnets + ", imagePath=" + imagePath
				+ ", user=" + user + ", approvedFlag=" + approvedFlag + ", creationDate=" + creationDate + "]";
	}
	
}
