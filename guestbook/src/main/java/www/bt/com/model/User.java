package www.bt.com.model;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

import www.bt.com.entity.Reviews;

@Component
public class User  {
	@NotNull(message="NotNull.user.userName")
	private String userName ;
	@NotNull
	private String password;
	public Long getUserID() {
		return userID;
	}

	public void setUserID(Long userID) {
		this.userID = userID;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public List<Reviews> getReviews() {
		return reviews;
	}

	public void setReviews(List<Reviews> reviews) {
		this.reviews = reviews;
	}


	private Long userID;
	private String role ;
	private String displayName ;
	private Date creationDate;
	private List<Reviews> reviews = new ArrayList<Reviews>();


	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserName() {
		return userName;
	}

	public String getPassword() {
		return password;
	}


	 @Override
	public String toString() {
		return "User [userName=" + userName + ", password=" + password + ", userID=" + userID + ", role=" + role
				+ ", displayName=" + displayName + ", creationDate=" + creationDate + ", reviews=" + reviews + "]";
	}
	
}
