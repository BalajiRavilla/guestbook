package www.bt.com.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
@NamedQueries({
	@NamedQuery(
	name = "findbyUsernameandPassword",
	query = " from User u ,Reviews r where u.userName = :userName and u.password=:password"
	)
})
@Entity
@Table(name="USERS")
public class User implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "USERID")
	private Long userID;
	public Long getUserID() {
		return userID;
	}
	@Column(name = "USERNAME")
	private String userName ;
	@Column(name = "PASSWORD")
	private String password;
	@Column(name = "ROLE")
	private String role ;
	@Column(name = "DISPLAY_NAME")
	private String displayName ;
	@OneToMany(
		        cascade = CascadeType.ALL,
		        orphanRemoval = true
		    )
	private List<Reviews> reviews = new ArrayList<Reviews>();
	  
	@Temporal(TemporalType.DATE)
	@Column(name = "DATE", nullable = true, length = 10)
	private Date creationDate;
	

	public void setUserID(Long userID) {
		this.userID = userID;
	}

	
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	public void setRole(String role) {
		this.role = role;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public void setReviews(List<Reviews> reviews) {
		this.reviews = reviews;
	}
	public String getDisplayName() {
		return displayName;
	}
	public String getPassword() {
		return password;
	}
	public String getUserName() {
		return userName;
	}
	public String getRole() {
		return role;
	}
	public List<Reviews> getReviews() {
		return reviews;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public Date getCreationDate() {
			return creationDate;
	}

	 /**
     * Constructs a <code>String</code> with all attributes
     * in name = value format.
     *
     * @return a <code>String</code> representation
     * of this object.
     */
    @Override
    public final String toString() {

	final String TAB = "  ";

    StringBuilder retValue = new StringBuilder();

    retValue.append("Login ( ")
        .append(super.toString()).append(TAB)
        .append("userName = ").append(this.userName).append(TAB)
        .append("password = ").append(this.password).append(TAB)
        .append("role = ").append(this.role).append(TAB)
        .append("userName = ").append(this.userName).append(TAB)
        .append(" )");

    return retValue.toString();
    }
	
}
