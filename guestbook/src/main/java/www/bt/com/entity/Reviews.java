package www.bt.com.entity;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
@Table(name="REVIEWS")
public class Reviews {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="REVIEW_ID")
    private Long reviewID;
	@Column(name = "REVIEW_COMMENTS")
    private String reviewCommnets;
    @Column(name = "IMAGE_PATH")
    private String imagePath;
    @Column(name = "APPROVED_FLAG")
    private char approvedFlag;
    @Temporal(TemporalType.DATE)
	@Column(name = "DATE", nullable = true, length = 10)
	private Date creationDate;
	
    public void setReviewID(Long reviewID) {
		this.reviewID = reviewID;
	}
	public void setReviewCommnets(String reviewCommnets) {
		this.reviewCommnets = reviewCommnets;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public void setApprovedFlag(char approvedFlag) {
		this.approvedFlag = approvedFlag;
	}
	
	public Long getReviewID() {
		return reviewID;
	}
	public String getReviewCommnets() {
		return reviewCommnets;
	}
	public String getImagePath() {
		return imagePath;
	}
	public char getApprovedFlag() {
		return approvedFlag;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public Date getCreationDate() {
			return creationDate;
	}
	@Override
	public String toString() {
		return "Reviews [reviewID=" + reviewID + ", reviewCommnets=" + reviewCommnets + ", imagePath=" + imagePath
				+ ", approvedFlag=" + approvedFlag + ", creationDate=" + creationDate + "]";
	}

    
}