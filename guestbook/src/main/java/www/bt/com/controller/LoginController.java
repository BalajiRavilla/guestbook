package www.bt.com.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import www.bt.com.dao.ReviewsDAO;
import www.bt.com.dao.LoginDAO;
import www.bt.com.model.User;


@Controller
public class LoginController {
	
	 @RequestMapping(value = "/login", method = RequestMethod.GET)
	  public String init(Model model) {
	    return "Login";
	  }
	 
	 @RequestMapping(value = "/Logout", method = RequestMethod.GET)
	  public String logout(Model model) {
	    return "Login";
	  }
	 @Autowired
	 LoginDAO loginDAO;
	 @Autowired
	 ReviewsDAO bookUploadDAO;
	 
	  @RequestMapping(value = "/processLogin", method = RequestMethod.POST)
	  public ModelAndView  login(ModelMap model, @ModelAttribute("user") @Validated  User user,BindingResult result) {
		  if(result.hasErrors()) {
	    	    return new ModelAndView("Login");
	        }
		 List<www.bt.com.entity.User> userData = loginDAO.login(user.getUserName(), user.getPassword());
		 if(userData.size() == 0 || userData.size() >1) {
			 model.addAttribute("message","Invalid userName or password entered"); 
			 return new ModelAndView("Login");
		 }
		 System.out.println(userData.toString());
		 user.setRole(userData.get(0).getRole());
		 user.setUserID(userData.get(0).getUserID());
		 user.setDisplayName(userData.get(0).getDisplayName());
		 user.setReviews(userData.get(0).getReviews());
		 //model.addAttribute("role",userData.get(0).getRole());
		 model.addAttribute("userData",user);
		 System.out.println("user"+user);
		 return new ModelAndView("Home");
		  
	
	  }
}
