package www.bt.com.controller;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import www.bt.com.dao.LoginDAO;
import www.bt.com.dao.ReviewsDAO;
import www.bt.com.dao.UserDao;
import www.bt.com.entity.Reviews;
import www.bt.com.entity.User;

@Controller
public class ReviewsController {
	 @Autowired
	 ReviewsDAO reviewsDAO;
	 @Autowired
	 UserDao userDao;
	 @Autowired
	 LoginDAO loginDAO;


	ModelAndView mv = new ModelAndView();
	
	 @RequestMapping(value = "/", method = RequestMethod.GET)
	    public ModelAndView showUploadForm(HttpServletRequest request,@ModelAttribute("user") User user,ModelMap model) {
		 List<www.bt.com.entity.User> userData = loginDAO.login(user.getUserName(), user.getPassword());
		 if(userData.size() == 0 || userData.size() >1) {
			 model.addAttribute("message","Invalid userName or password entered"); 
			 return new ModelAndView("Login");
		 }
		 System.out.println(userData.toString());
		 user.setRole(userData.get(0).getRole());
		 user.setUserID(userData.get(0).getUserID());
		 user.setDisplayName(userData.get(0).getDisplayName());
		 user.setReviews(userData.get(0).getReviews());
		 //model.addAttribute("role",userData.get(0).getRole());
		 model.addAttribute("userData",user);
		
		 return new ModelAndView("Home");

	    }
	
	 
	 @RequestMapping(value = "/add_reviews", method = RequestMethod.POST)
	 public ModelAndView login(HttpServletRequest request, @RequestParam("filename") MultipartFile file, 
			 RedirectAttributes redirectAttributes, ModelMap model)throws Exception  {
		 String reviewCommnets= request.getParameter("review");
		 String userId= request.getParameter("userId");
		 String userName= request.getParameter("userName");
		 String SAVE_DIR="WEB-INF/images";
		 Reviews reviews = new Reviews();
		 if (file.isEmpty()) {
	            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
	            return new ModelAndView("redirect:Home");
	        }
		 
		 // gets absolute path of the web application
	      String appPath = request.getServletContext().getRealPath("");
	     System.out.println("appPath="+appPath);
	     System.out.println(file.getOriginalFilename());
	     String savePath = appPath + File.separator + SAVE_DIR;   
	     File fileSaveDir = new File(savePath);
	        if (!fileSaveDir.exists()) {
	            fileSaveDir.mkdir();
	        }

		byte[] bytes = file.getBytes();
		BufferedOutputStream bout= new BufferedOutputStream(new FileOutputStream(
				new File(fileSaveDir+File.separator+file.getOriginalFilename())));
		bout.write(bytes);
		bout.flush();  
        bout.close();  
          
		User user = new User();
		System.out.println("userId"+userId);
		user.setUserID(Long.parseLong(userId));
		user.setUserName(userName);
		reviews.setReviewCommnets(reviewCommnets);
		reviews.setImagePath(file.getOriginalFilename());
        //fileUpload.setUser(user);
		reviews.setCreationDate(new Date());
		List<Reviews> l= new ArrayList<Reviews>();
				l.add(reviews);
        user.setReviews(l);
        //fileUpload.setFilePath(SAVE_DIR+File.separator+file.getOriginalFilename());
       // bookUploadDAO.save(fileUpload);
		this.userDao.save(user);

        model.addAttribute("SuccessUpload","File has been saved sucessfully");
        return new ModelAndView("Home");
        
	  }

	 @RequestMapping(value="manage_reviews", method = RequestMethod.POST)
	 public ModelAndView approve(HttpServletRequest request,@RequestParam("submitButton") String action)throws Exception  {
		 
		 Reviews reviews = new Reviews();
		 String review=request.getParameter("review");
		 String reviewID=request.getParameter("reviewID");
		 reviews.setReviewID(Long.parseLong(reviewID));
		 reviews.setReviewCommnets(review);
		 System.out.println(reviews);
          if(action.equals("Approve"))   {
     		 System.out.println("update query");

     		reviewsDAO.update(reviews);
          }
          
          if(action.equals("Delete"))   {
        	  
        	  reviewsDAO.delete(reviews); 
          }
	        return new ModelAndView("Home");

      
	  //return "Success";
}  
 }